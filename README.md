# BotnetDetectorsComparer

This is a program to compare different botnet/malware detectors based on network traffic. The idea is to read a netflow file that has a new column for each prediction of an algorithm and compare how each algorithm detects the traffic.
It computes the FP, FN, TP and TN for each flow in a time window, by counting the errors per IP address. At the end of each time window several performance metrics are compared, and also at the end of the capture.


# Usage

To use it you should give a binetflow file, the type of comparison and the width of the time window.

    ./BotnetDetectorsComparer.py -f statisticGenerator.testcasewithheaders9.txt -t weight -T 300

Giving an alpha is also a good idea, if not the program will assume a default of 0.01 (like in our experiments)

With -p it will plot and open a window with the graph information for each method. With -P <format> it will store the plots on disk. (format is in the help, but can by almost anything like png)


Any problem contact sebastian.garcia@agents.fel.cvut.cz or eldraco@gmail.com

# Options

    usage: ./BotnetDetectorsComparer.py <options>
    options:
      -h, --help           Show this help message and exit
      -V, --version        Output version information and exit
      -v, --verbose        Output more information.
      -D, --debug          Debug. In debug mode the statistics run live.
      -f, --file           SORTED input netflow labeled file to analyze (Netflow or Argus).
      -t, --type           Type of comparison. Flow based (-t flow), time based (-t time), or weighted (-t weight). The weighted type is the new IP-based and time-based error metric.
      -T, --time           While using time based comparison, specify the time window to use in secodns. E.g. -T 120
      -p, --plot           Plot the fmeasures of all methods and show them on the display.
      -a, --alpha          In weight mode, use this alpha for computing the score (defaults to 0.4).
      -c, --cvs            Print the final scores in cvs format into the specified file. E.g. -c results.csv
      -o, --out            Store in a log file everything that is shown in the screen. -o all-info.txt
      -P, --plot-to-file   Instead of showing the plot on the screen, store it in a file. Type of plot given by the file extension. E.g. -P all-info.eps. The name of each algorithm is added at the beginning of the file name.

